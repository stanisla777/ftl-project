const gulp = require('gulp');
const autopref = require('gulp-autoprefixer');
const sync = require('browser-sync'); // Подключаем Browser Sync
const prepless = require('gulp-less');//преобразует less в css
const cssmin = require('gulp-clean-css');
const renamed = require('gulp-rename');//преобразует файлы в min
const svgsprite = require('gulp-svg-sprite');
const include = require('gulp-file-include');
const del = require('del')//удалять файлы
const htmlmin = require('gulp-htmlmin')//минифицировать html
const jsmin = require('gulp-jsmin')//минифицировать js
const babel = require('gulp-babel');
const imagemin = require('gulp-imagemin');//оптимизировать изображение
const imgCompress  = require('imagemin-jpeg-recompress');
const webp  = require('gulp-webp');
const webhtml  = require('gulp-webp-html');
const ttfwolf  = require('gulp-ttf2woff');
const ttfwolf2  = require('gulp-ttf2woff2');
const ttfeot  = require('gulp-ttf2eot');
const ttfsvg  = require('gulp-ttf-svg');
const gcmq = require('gulp-group-css-media-queries');
const htmlreplace = require('gulp-html-replace');


//gulp serve - разработка
//gulp build - production
//gulp production - production min

//Переменные
const fs = require('fs')
const project_folder = require('path').basename(__dirname)

function replacehtml() {
  return gulp.src('dist/**.html')
    .pipe(htmlreplace({
      'css': './css/style.min.css',
      'media': './css/media.min.css',
      'js': './js/app.min.js',

    }))
    .pipe(gulp.dest('dist'))
}
exports.replacehtml = replacehtml



function media_group() {
  return gulp.src('dist/css/style.css')
    .pipe(gcmq())
    .pipe(gulp.dest('mist/css/style.css'));
}
exports.media_group = media_group




//Собираем index.html из разных файлов или просто копируем все html в папку dist
function html() {
  return gulp.src('src/**.html')
    .pipe(include({
      prefix:'@@'
    }))
    // .pipe(webhtml()) //по необходимости, е нужно подставлять img.webp
    .pipe(htmlmin({
      collapseWhitespace:true,//сдлелали html.min
      preserveLineBreaks:true//убрали просто пробелы
    }))

    .pipe(gulp.dest('dist'))
}
exports.html = html

//Собираем js из разных файлов или просто копируем все js в папку dist
function js() {
  return gulp.src('src/js/app.js')
    .pipe(include({
      prefix:'@@'
    }))
    .pipe(babel({
        presets: ['@babel/env']
      }
    ))
    .pipe(gulp.dest('dist/js'))

  // ниже если нужно будет деает min.js
  // .pipe(jsmin())
  // .pipe(renamed({extname: '.min.js'}))
  // .pipe(gulp.dest('dist/js'))
}
exports.js = js

function js_min() {
  return gulp.src('src/js/app.js')
    .pipe(include({
      prefix:'@@'
    }))
    .pipe(babel({
        presets: ['@babel/env']
      }
    ))
    .pipe(gulp.dest('dist/js'))

  .pipe(jsmin())
  .pipe(renamed({extname: '.min.js'}))
  .pipe(gulp.dest('dist/js'))
}
exports.js_min = js_min

//Просто копирум js файлы
function jsCopy() {
  return gulp.src(['src/js/**.js','!src/js/app.js'])
    .pipe(gulp.dest('dist/js'))

}
exports.jsCopy = jsCopy

//Собираем единый css из нескольких файлов

function allcss() {
  gulp.src(['src/css/**.css','!src/css/**.less'])
    .pipe(cssmin())
    .pipe(renamed({extname: '.min.css'}))
    .pipe(gulp.dest('dist/css'))
  return gulp.src('src/css/style.less')
    .pipe(include({
      prefix:'@@'
    }))
    .pipe(prepless())
    .pipe(autopref({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gcmq())
    // .pipe(webcss())//по необходимости, если нужно подставлять в css img.webp
    .pipe(cssmin())
    .pipe(renamed({extname: '.min.css'}))
    .pipe(gulp.dest('dist/css'))

}



//преобразуем less в css
function less() {
  gulp.src(['src/partcss/**','!src/css/**.less'])
    .pipe(gulp.dest('dist/partcss'))
  gulp.src(['src/css/**','!src/css/**.less'])
    .pipe(gulp.dest('dist/css'))
  return gulp.src('src/css/style.less')
    .pipe(include({
      prefix:'@@'
    }))
    .pipe(prepless())
    .pipe(autopref({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gcmq())
    // .pipe(webcss())//по необходимости, если нужно подставлять в css img.webp
    .pipe(gulp.dest('dist/css'))
  // ниже если нужно будет деает style.css
  // .pipe(cssmin())
  // .pipe(renamed({extname: '.min.css'}))
  // .pipe(gulp.dest('dist/css'))
}
exports.less = less

// Добавление префиксов в css при перемещение
function media_css() {
  return gulp.src('src/css/media.css')
    .pipe(autopref({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('dist/css'))
}
exports.media_css = media_css



//Конвертация шрифтов
function convertFont() {
  gulp.src('src/fonts/**/**.ttf')
    .pipe(gulp.dest('dist/fonts'))
    .pipe(ttfwolf())
    .pipe(gulp.dest('src/fonts'))
    .pipe(gulp.dest('dist/fonts'))
  gulp.src('src/fonts/**/**.ttf')
    .pipe(ttfwolf2())
    .pipe(gulp.dest('dist/fonts'))
    .pipe(gulp.dest('src/fonts'))
  gulp.src('src/fonts/**/**.ttf')
    .pipe(ttfsvg())
    .pipe(gulp.dest('dist/fonts'))
    .pipe(gulp.dest('src/fonts'))
  return gulp.src('src/fonts/**/**.ttf')
    .pipe(ttfeot())
    .pipe(gulp.dest('src/fonts'))
    .pipe(gulp.dest('dist/fonts'))
}
exports.convertFont = convertFont

//копирование и оптимизация картинок
function img() {
  return gulp.src('src/img/**.{jpg,jpeg,png,svg,gif,ico,webp,mp4,ogv,webm}')
    .pipe(gulp.dest('dist/img'))
}
exports.img = img

//Удаляем папки svgicon в src и dist
function delSvgIconSrc() {
  return del('src/img/svgicon/sprite.svg')
}
exports.delSvgIconSrc=delSvgIconSrc
function delSvgIconDist() {
  return del('dist/img/svgicon')
}
exports.delSvgIconDist=delSvgIconDist

//Создаем спрайт из svg
function createSprite() {
  return gulp.src('src/img/svgicon/*.svg')
    .pipe(svgsprite({
        shape: {
          dimension: { // Set maximum dimensions
            // maxWidth: 200,
            // maxHeight: 200
          },
          spacing: { // Add padding
            // padding: 5
          }
        },
        mode: {
          stack: {
            sprite: "../sprite.svg"  //sprite file name
          }
        },
      }
    ))
    .pipe(gulp.dest('src/img/svgicon'))
    .pipe(gulp.dest('dist/img/svgicon'))
}


exports.svgSprire = gulp.series(delSvgIconSrc,delSvgIconDist,createSprite,img)

//очищать папку dist
function clear() {
  return del('dist')
}
exports.clear=clear

//продакшен, но без минимизации объединения css в один файл
exports.build = gulp.series(clear,less,html,js,jsCopy,delSvgIconSrc,delSvgIconDist,createSprite,img,convertFont)

// продакшен с минимизацией и объединением css файлов в один

exports.production = gulp.series(clear,allcss,html,js_min,jsCopy,delSvgIconSrc,delSvgIconDist,createSprite,img,convertFont,replacehtml)

//перезагрузка страницы при изменении в html или css
function serve() {
  sync.init({
    server:'./dist'
  })
  gulp.watch('src/**.html',gulp.series(html)).on('change',sync.reload)
  gulp.watch('src/css/**.less',gulp.series(less)).on('change',sync.reload)
  gulp.watch('src/css/**.css',gulp.series(media_css)).on('change',sync.reload)
  gulp.watch('src/img/**.{jpg,jpeg,png,svg,gif,ico,webp,mp4,ogv,webm}',gulp.series(img)).on('change',sync.reload)
  gulp.watch('src/js/app.js',gulp.series(js)).on('change',sync.reload)
  gulp.watch('src/js/**.js',gulp.series(jsCopy)).on('change',sync.reload)
  gulp.watch('src/img/**.{jpg,jpeg,png,svg,gif,ico,webp}',gulp.series(img)).on('change',sync.reload)
  gulp.watch(['src/img/svgicon/**.svg','!src/img/svgicon/sprite.svg'],gulp.series(delSvgIconSrc,delSvgIconDist,createSprite)).on('change',sync.reload)
}

//разработка
exports.serve = gulp.series(clear,less,html,js,jsCopy,delSvgIconSrc,delSvgIconDist,createSprite,img,convertFont,serve)



//Картинки преобразуем в формат webp

function imgwebp() {
  return gulp.src('src/img/**.{jpg,jpeg,png}')
    .pipe(gulp.dest('dist/img'))
    .pipe(webp({
      quality:70
    }))
    .pipe(gulp.dest('dist/img'))
}
exports.imgwebp=imgwebp



