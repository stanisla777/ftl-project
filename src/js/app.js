

window.onload=function(){
  @@include('parts/closest.js')
  @@include('parts/autosize.min.js')

  const drop_button = document.querySelectorAll('.dropdown-button')
  const drop_buttonArray= Array.prototype.slice.call(drop_button);
  const dropdown_list = document.querySelectorAll('.dropdown-list')
  drop_buttonArray.forEach(function (item,ind,arr) {
    item.onclick = function () {

      const child = item.children
      for(let index = 0; index < child.length; index++){
        if(child[index].classList.contains('check-mark')){
          child[index].classList.toggle('active')
        }
      }

      const parent = item.closest('.ftl-form__col')
      const elems = parent.children

      for(let index = 0; index < elems.length; index++){
        if(elems[index].classList.contains('dropdown-list')){
          if (elems[index].classList.contains('active')) {
            elems[index].classList.remove('active');
            elems[index].style.maxHeight = 0;
          } else {
            elems[index].classList.add('active');
            elems[index].style.maxHeight = elems[index].scrollHeight + 'px';
          }
        }
      }
    }
  })

  const list__item = document.querySelectorAll('.dropdown-list__item')
  const list__itemArray= Array.prototype.slice.call(list__item);
  list__itemArray.forEach(function (item,index,arr) {
    item.onclick = function () {
      const text = this.textContent
      const parent = item.closest('.ftl-form__col')
      const elems = parent.children
      console.log(elems);
      for(let index = 0; index < elems.length; index++){
          if (elems[index].classList.contains('dropdown-button')) {
            const child = elems[index].children
            for(let index = 0; index < child.length; index++){
              if (child[index].classList.contains('ftl-form__val')) {
                child[index].classList.add('active')
                child[index].textContent = text;

              }
              if (child[index].classList.contains('check-mark')) {
                child[index].classList.remove('active')
              }
            }
          }
        if (elems[index].classList.contains('ftl-form__input-hidden')) {
          elems[index].value = text
        }
          if(elems[index].classList.contains('dropdown-list')){
            elems[index].classList.remove('active');
            elems[index].style.maxHeight = 0;
          }
      }
    }
  })

  const textarea = document.querySelector('.ftl-form__textarea')
  autosize(textarea);

  const check = document.querySelector('#checkbox')
  const block = document.querySelector('.temperature-regime')

  check.onchange=function () {
    if (check.checked) {
      block.classList.add('active')
    }
    else {
      block.classList.remove('active')
    }
  }

}
